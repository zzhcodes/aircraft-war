/*
 * @Description: 面板基类
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-03 22:50:06
 * @LastEditors: 周曾辉
 * @LastEditTime: 2020-04-25 22:38:16
 */

import { PanelType, PanelId } from "../Const/PanelValue";
import Utils from "../Utils/Utils";
import EventManager from "../Manager/EventManager";


const {ccclass, property} = cc._decorator;

@ccclass
export default class BasePanel extends cc.Component {

    _panelType:PanelType;

    _panelId:PanelId;

    _data:any = null;
    
    @property(cc.Node)
    closeBtn:cc.Node = null;

    constructor(...argvs){
        super();
    }

    public getPanelType():PanelType{
        return this._panelType;
    }

    public getPanelId():PanelId{
        return this._panelId;
    }


    onLoad () {
        //弹出式窗口  Window
        if(this._panelType === PanelType.Window){
            Utils.loadPrefab('BlackBg',null,null,this.node,-999);

            let content = this.node.getChildByName("content");
            if(content){
                content.scale = 0;
                content.runAction(cc.sequence(cc.scaleTo(0.3,1.2,1.2),cc.scaleTo(0.2,1,1)).easing(cc.easeInOut(3.0)));
            }
        }

        if(this.closeBtn){
            this.closeBtn.on('click', this.close, this);
        }
    }

    setData(data:any){
        this._data = data;
    }

    bindEvent(evtName:string,callFunc:Function){
        EventManager.getInstance().on(evtName,callFunc,this);
    }

    show(){
        this.node.active = true;
    }

    hide(){
        if(this._panelType === PanelType.Window){
            let content = this.node.getChildByName("content");
            if(content){
                let callFunc = cc.callFunc(()=>{
                    this.node.active = false;
                })
                content.runAction(cc.sequence(cc.scaleTo(0.1,1.2,1.2),cc.scaleTo(0.2,0,0),callFunc));
            }
            
        }else{
            this.node.active = false;
        }
    }

    close(){
        if(this._panelType === PanelType.Window){
            let content = this.node.getChildByName("content");
            if(content){
                let callFunc = cc.callFunc(()=>{
                    this.node.active = false;
                })
                content.runAction(cc.sequence(cc.scaleTo(0.1,1.2,1.2),cc.scaleTo(0.2,0,0),callFunc));
            }
        }else{
            this.node.active = false;
        }
        
    }

    onDestroy(){
        super.onDestroy();
        EventManager.getInstance().offTarget(this);
    }
}
