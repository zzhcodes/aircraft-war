/*
 * @Description: 
 * @version: 
 * @Author: CoderXZ
 * @Date: 2020-04-08 08:22:04
 * @LastEditors: CoderXZ
 * @LastEditTime: 2020-05-17 12:41:53
 */ 


const { ccclass, property } = cc._decorator;

@ccclass
export default class ScrollViewPro extends cc.ScrollView {

    // LIFE-CYCLE CALLBACKS:


    itemPool:cc.NodePool;

    start() {

    }

    onLoad() {
        this.optDC();
        this.node.on('scrolling', this.optDC, this);
    }

    optDC(){
        let self = this;
        let svLeftBottomPt:cc.Vec2 = this.node.parent.convertToWorldSpaceAR(new cc.Vec2(
            self.node.x - self.node.anchorX * self.node.width, self.node.y - self.node.anchorY * self.node.height
        ));

        var viewRect = cc.rect(svLeftBottomPt.x,svLeftBottomPt.y,this.node.width, this.node.height);
        for (let i = 0; i < this.content.children.length; i++) {
            const node = this.content.children[i];
            if (viewRect.intersects(node.getBoundingBoxToWorld())) {
                node.opacity = 255;
            }
            else {
                node.opacity = 0;
            }
        }
    }


    // update(dt) {
    // }
}
