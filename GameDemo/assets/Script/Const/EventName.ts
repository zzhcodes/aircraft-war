/*
 * @Description: 平台常量字符串值
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-03 21:02:11
 * @LastEditors: CoderXZ
 * @LastEditTime: 2020-05-31 17:17:10
 */

export enum EventName{
    START_GAME = "START_GAME",//开始游戏
    ENEMY_KILLED = "ENEMY_KILLED",//敌人被杀死
    GAME_OVER = "GAME_OVER",//游戏结束
    SHOW_PANEL = "SHOW_PANEL",//显示面板
    CONTINUE_GAME = "CONTINUE_GAME",//继续游戏
    PAUSE_GAME = "PAUSE_GAME",//暂停游戏
    UPDATE_KILLNUM = "UPDATE_KILLNUM",//更新杀敌人数
    SHOW_LOADING = "SHOW_LOADING",//显示Loading
    HIDE_LOADING = "HIDE_LOADING",//隐藏Loading
    PASS_CHECKPOINT = "PASS_CHECKPOINT",//通过当前关卡
    PASS_ALL = "PASS_ALL",//完全通关
}
