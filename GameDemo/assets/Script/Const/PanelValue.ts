/*
 * @Description: 
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-07 23:09:16
 * @LastEditors: 周曾辉
 * @LastEditTime: 2020-04-15 23:22:12
 */



 export enum PanelType{
     Panel = 1,
     Window,
     None
 }

 export enum PanelId{
     MainPanel = 1,
     PopWindow,
     LoadingDlg,
 }

 export let PanelName:any = {
     1:"MainPanel",
     2:"PopWindow",
     3:"LoadingDlg"
 }
