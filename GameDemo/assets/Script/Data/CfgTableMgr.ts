/*
 * @Description: 
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-19 22:15:06
 * @LastEditors: 周曾辉
 * @LastEditTime: 2020-04-19 23:04:40
 */

import MainPanel from "../View/Layer/MainPanel";


const {ccclass, property} = cc._decorator;

@ccclass
export default class CfgTableMgr {

    private static m_instance:CfgTableMgr = null;
    
    private m_tableCfgMap:any = {};

    public static getInstance():CfgTableMgr{
        if(!this.m_instance){
            this.m_instance = new CfgTableMgr();
        }

        return this.m_instance;
    }

    private constructor(){
        this.m_tableCfgMap = {};
    }

    initTable(tabName:string,data:any){
        this.m_tableCfgMap[tabName] = data;
    }

    getTableData(tabName:string){
        return this.m_tableCfgMap[tabName];
    }
}
