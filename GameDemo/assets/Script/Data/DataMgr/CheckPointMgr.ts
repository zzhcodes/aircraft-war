/*
 * @Description: 
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-22 23:02:40
 * @LastEditors: 周曾辉
 * @LastEditTime: 2020-04-25 22:22:13
 */

import UserData from "../../UserData/UserData";
import CfgTableMgr from "../CfgTableMgr";
import EventManager from "../../Manager/EventManager";
import { EventName } from "../../Const/EventName";
import CheckPointModel from "../Model/CheckPointModel";


const {ccclass, property} = cc._decorator;

@ccclass
export default class CheckPointMgr {

    private static m_instance:CheckPointMgr = null;

    private m_curCheckPointNum:number = 0;

    private m_checkPointModels:CheckPointModel[] = [];

    public static getInstance():CheckPointMgr{
        if(!this.m_instance){
            this.m_instance = new CheckPointMgr();
        }

        return this.m_instance;
    }

    private constructor(){
        this.initData();   
        this.initEvent();
    }

    private initData(){
        let checkPointNum = UserData.getInstance().getCheckPointNum();
        if(checkPointNum){
            this.m_curCheckPointNum = checkPointNum;
        }

        let checkPointCfgData:any = CfgTableMgr.getInstance().getTableData("CheckPoint");
        if(!checkPointCfgData){
            return;
        }

        for(let key in checkPointCfgData){
            if(checkPointCfgData.hasOwnProperty(key)){
                let checkPointModel = new CheckPointModel();
                checkPointModel.init(checkPointCfgData[key]);
                checkPointModel.setId(Number(key));
                this.m_checkPointModels.push(checkPointModel);
            }
        }
        this.m_checkPointModels.sort(this.sortFunction.bind(this));
    }

    sortFunction(itemA:CheckPointModel,itemB:CheckPointModel){
        if(itemA.getId() - itemB.getId()){
            return true;
        }
    }

    private initEvent(){
        EventManager.getInstance().on(EventName.UPDATE_KILLNUM,this.checkToNextCheckPoint.bind(this),this);
    }

    public getCurCheckPointNum():number{
        return this.m_curCheckPointNum;
    }

    public setCurCheckPointNum(num:number):void{
        if(this.m_curCheckPointNum === num){
            return;
        }
        this.m_curCheckPointNum = num;
    }

    public getEnemySpeed(){
        let checkPointData = CfgTableMgr.getInstance().getTableData("CheckPoint");
        if(!checkPointData){
            return 0;
        }

        let curData = checkPointData[this.m_curCheckPointNum];
        return curData['enemy_speed'];
    }

    public getKillGoal():number{
        let checkPointData = CfgTableMgr.getInstance().getTableData("CheckPoint");
        if(!checkPointData){
            return 0;
        }

        let curData = checkPointData[this.m_curCheckPointNum];
        return curData['kill_num'];
    }

    public getBulletSpeed():number{
        let checkPointData = CfgTableMgr.getInstance().getTableData("CheckPoint");
        if(!checkPointData){
            return 0;
        }

        let curData = checkPointData[this.m_curCheckPointNum];
        return curData['bullet_speed'];
    }


    // UPDATE_KILLNUM
    public checkToNextCheckPoint():void{
        let curKillNum:number = UserData.getInstance().getKillEnemyNum();
        let goal = this.getKillGoal();
        if(curKillNum >= goal){
            EventManager.getInstance().emit(EventName.PASS_CHECKPOINT,this.getCurCheckPointNum());
        }
    }

    public etMaxCheckPointNum():number{
        let length = this.m_checkPointModels.length;
        let maxCheckPointModel = this.m_checkPointModels[length - 1];
        return maxCheckPointModel.getId();
    }
}
