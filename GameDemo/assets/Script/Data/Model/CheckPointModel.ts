/*
 * @Description: 
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-25 21:33:20
 * @LastEditors: 周曾辉
 * @LastEditTime: 2020-04-25 21:46:22
 */


const {ccclass, property} = cc._decorator;

@ccclass
export default class CheckPointModel {
    private m_Id:number = 0;
    private m_enemySpeed:number = 0;
    private m_bulletSpeed:number = 0;
    private m_killGoal:number = 0;

    constructor(...argvs){
        
    }

    init(cfgData:any){
        this.m_bulletSpeed = cfgData['bullet_speed'];
        this.m_enemySpeed = cfgData['enemy_speed'];
        this.m_killGoal = cfgData['kill_num'];
    }

    public getId():number{
        return this.m_Id;
    }

    public setId(id:number):void{
        this.m_Id = id;
    }

    public getEnemySpeed():number{
        return this.m_enemySpeed;
    }

    public getBulletSpeed():number{
        return this.m_bulletSpeed;
    }

    public getKillGoal():number{
        return this.m_killGoal;
    }

}
