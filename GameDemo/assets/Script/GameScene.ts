/*
 * @Description: 
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-03 21:02:11
 * @LastEditors: 周曾辉
 * @LastEditTime: 2020-04-15 23:16:46
 */
import SoundManager from "./Manager/SoundManager";
import EventManager from "./Manager/EventManager";
import Global from "./Global";
import Utils from "./Utils/Utils";
import { EventName } from "./Const/EventName";
import { PanelId } from "./Const/PanelValue";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameScene extends cc.Component {

    start() {
        // init logic

    }

    onLoad() {
        this.initEvents();
        this.initView();

        // EventManager.getInstance().emit(EventName.SHOW_LOADING);
    }


    initEvents(){
        EventManager.getInstance().on(EventName.SHOW_LOADING,()=>{
            cc.log("show loading");
            EventManager.getInstance().emit(EventName.SHOW_PANEL,{panelId:PanelId.LoadingDlg});
        },this);
    }

    initView(){
        EventManager.getInstance().emit(EventName.SHOW_PANEL,{panelId:PanelId.MainPanel});
    }

    onDestroy(){
        super.onDestroy();
        EventManager.getInstance().offTarget(this);
    }

}
