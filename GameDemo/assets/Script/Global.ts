/*
 * @Description: 
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-03 21:02:11
 * @LastEditors: CoderXZ
 * @LastEditTime: 2020-05-28 22:34:59
 */


export default class Global{

    public static m_Formal:boolean = false;//正式服还是测试服

    public static m_debugFlag:boolean = false;

    public static m_fps:number = 60;

    public static m_version_name:string = "1.0.0";

    public static m_userData:any = {};

    public static platform:string = "wechat";


    public static getHttpUrlByChannelName(){
        if (Global.m_Formal){
            return "http://116.62.15.182:8585/api.php"
        }else{
            return "http://api.taiheja.com/api.php"
        }
    }

    public static onSendUrl(data:any, ex_data:any){
        if (!ex_data.ignore_ver){
            let m_ver = Global.m_version_name;
            data.ver = m_ver
        }
        
        if (Global.m_userData){
            let uid = Global.m_userData.uid;
            let skey = Global.m_userData.skey;
            let app_id = Global.m_userData.app_id;
            data.uid = uid;
            data.skey = skey;
            data.app_id = app_id;
        }
    }

}