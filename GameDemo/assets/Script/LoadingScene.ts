/*
 * @Description: 
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-06 16:01:20
 * @LastEditors: CoderXZ
 * @LastEditTime: 2020-09-07 22:52:50
 */

import Global from "./Global";
import SceneManager from "./Manager/SceneManager";
import GlobalFunction from "./Utils/GlobalFunction";
import SoundManager from "./Manager/SoundManager";
import EventManager from "./Manager/EventManager";
import PanelManager from "./Manager/PanelManager";
import ResManager from "./Manager/ResManager";
import SdkManager from "./Manager/SdkManager";
import UserData from "./UserData/UserData";
import { EventName } from "./Const/EventName";
import CfgTableMgr from "./Data/CfgTableMgr";
import CheckPointMgr from "./Data/DataMgr/CheckPointMgr";
import { PlatformValue } from "./SDK/PlatformValue";
import NativeForAndroid from "./SDK/NativeForAndroid";


const { ccclass, property } = cc._decorator;

@ccclass
export default class LoadingScene extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property(cc.ProgressBar)
    progressBar: cc.ProgressBar = null;

    _time: number = 0;

    _bEnterGameScene: boolean = false;

    _cfgLoaded: boolean = false;

    m_platform: string = "";

    start() {

    }

    onLoad() {
        cc.log("Loading Scene loaded!");
        cc.log(new Date().toString());
        cc.debug.setDisplayStats(Global.m_debugFlag);
        cc.game.setFrameRate(Global.m_fps);

        this.initData();

        this.bootMgr();

        this.initView();

        this.downloadCfg(() => {
            this._cfgLoaded = true;
        });
    }

    initData() {
        this._bEnterGameScene = false;
        this._cfgLoaded = false;
    }

    initView() {

        this.m_platform = SdkManager.getInstance().getPlatformStr();
        console.log("this.m_platform:",this.m_platform);
        if (this.m_platform === PlatformValue.WECHAT) {
            let button = wx.createUserInfoButton({
                type: 'text',
                text: '进入游戏',
                style: {
                    left: 100,
                    top: 400,
                    width: 120,
                    height: 40,
                    lineHeight: 40,
                    backgroundColor: '#ff0000',
                    color: '#ffffff',
                    textAlign: 'center',
                    fontSize: 16,
                    borderRadius: 4
                }
            })
            button.onTap((res) => {
                console.log(res);
                // let userInfo:UserInfo = {};
                
                // UserData.getInstance().setUserInfo()
            })
        }

       if(cc.sys.platform === cc.sys.ANDROID){
        //    NativeForAndroid.showLog("我是Log Log");
       }

    }

    downloadCfg(callBack: Function = null) {
        cc.loader.loadRes("configs/CheckPoint", (err, obj) => {
            if (err) {
                cc.log("加载配置表CheckPoint.json失败");
                return;
            }

            let tabName = obj.name;
            let tableData = obj.json.data;
            CfgTableMgr.getInstance().initTable(tabName, tableData);
            cc.log("加载配置表" + tabName + "完毕");
            if (callBack) {
                callBack();
            }
        })
    }

    //初始化管理类
    bootMgr() {
        CfgTableMgr.getInstance();
        UserData.getInstance();

        SceneManager.getInstance();
        SoundManager.getInstance();
        EventManager.getInstance();
        PanelManager.getInstance();
        ResManager.getInstance();
        SdkManager.getInstance();

        CheckPointMgr.getInstance();

    }

    update(dt) {

        if (this._bEnterGameScene) {
            return;
        }

        let total_seconds = Global.m_fps * 1;
        this._time++;
        this.progressBar.progress = this._time / total_seconds;
        this.label.string = "玩命加载中" + GlobalFunction.getPercentStr(this._time, total_seconds) + "...";
        if (this._time >= total_seconds && !this._bEnterGameScene && this._cfgLoaded) {

            this.progressBar.progress = 1;
            this.label.string = "加载完成，进入游戏！";

            // if (this.m_platform === PlatformValue.WINDOWS) {
                this.scheduleOnce(()=>{
                    SceneManager.getInstance().gotoScene("GameScene");
                },0.1);
            // }

            this._bEnterGameScene = true;
            cc.log(new Date().toString());
            return;
        }
    }
}
