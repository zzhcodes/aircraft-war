/*
 * @Description: 面板管理器
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-03 22:47:03
 * @LastEditors: 周曾辉
 * @LastEditTime: 2020-04-15 23:50:08
 */

import EventManager from "./EventManager";
import { EventName } from "../Const/EventName";
import BasePanel from "../Component/BasePanel";
import { PanelName, PanelId } from "../Const/PanelValue";
import ResManager from "./ResManager";
import Utils from "../Utils/Utils";


export default class PanelManager{

    private static m_instance:PanelManager = null;

    panelQueue:PanelId[] = [];

    public static getInstance():PanelManager{
        if(!this.m_instance){
            this.m_instance = new PanelManager();
        }

        return this.m_instance;
    }

    private constructor(){
        EventManager.getInstance().on(EventName.SHOW_PANEL,this.onShowPanel.bind(this));
    }

    private onShowPanel(panelData:any){
        let panelId = panelData["panelId"];
        let panelName = PanelName[panelId];

        let data = panelData['data'];

        var callBack = ()=>{
            let index = this.panelQueue.indexOf(panelId);
            if(index >= 0){
                this.panelQueue.push(panelId);
            }
        }

        if(data){
            Utils.loadPrefab(panelName,callBack,data);
        }else{
            Utils.loadPrefab(panelName,callBack);
        }
        
    }
}
