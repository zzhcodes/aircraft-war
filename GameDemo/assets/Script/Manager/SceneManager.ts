/*
 * @Description: 场景管理器
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-03 21:02:11
 * @LastEditors: 周曾辉
 * @LastEditTime: 2020-04-03 22:52:16
 */


export default class SceneManager{

    private static m_instance:SceneManager = null;

    public static getInstance():SceneManager{
        if(!this.m_instance){
            this.m_instance = new SceneManager();
        }

        return this.m_instance;
    }

    private constructor(){

    }

    //切换场景
    gotoScene(sceneName:string){
        cc.director.loadScene(sceneName);
    }

    //预加载场景
    preloadScene(sceneName:string){
        cc.director.preloadScene(sceneName);
    }
}
