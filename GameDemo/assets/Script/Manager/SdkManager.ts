/*
 * @Description: SDK管理器
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-03 21:02:11
 * @LastEditors: CoderXZ
 * @LastEditTime: 2020-05-28 23:11:07
 */
import { PlatformValue } from "../SDK/PlatformValue";
import BaseSdkCtrl from "../SDK/BaseSdkCtrl";
import WechatApi from "../SDK/WechatApi";
import BaiduApi from "../SDK/BaiDuApi";
import QQApi from "../SDK/QQApi";



export default class SdkManager {
    private static m_instance:SdkManager = null;
    private sdkCtrl:BaseSdkCtrl = null;

    public static getInstance():SdkManager{
        if(!this.m_instance){
            this.m_instance = new SdkManager();
        }

        return this.m_instance;
    }

    private constructor(){
        this.initSDk();
    }

    private initSDk():void{

        let platformStr:string = this.getPlatformStr();
        if(platformStr === PlatformValue.WECHAT){
            this.sdkCtrl = new WechatApi();
        }else if(platformStr === PlatformValue.BAIDU){
            this.sdkCtrl = new BaiduApi();
        }else if(platformStr === PlatformValue.QQ){
            this.sdkCtrl = new QQApi();
        }else{
            this.sdkCtrl = new WechatApi();
        }

    }


    public getPlatformStr():string{
        let platformStr:string = PlatformValue.WECHAT_SMALLGAME;
        let platform = cc.sys.platform;
        let os = cc.sys.os;
        if(platform === cc.sys.WECHAT_GAME){
            platformStr = PlatformValue.WECHAT;//微信小游戏
        }else if(platform === cc.sys.BAIDU_GAME){
            platformStr = PlatformValue.BAIDU;//百度小游戏
        }else if(platform === cc.sys.QQ_PLAY){
            platformStr = PlatformValue.QQ;//QQ小游戏
        }else if(platform === cc.sys.XIAOMI_GAME){
            platformStr = PlatformValue.XIAOMI;//小米小游戏
        }else if(platform === cc.sys.HUAWEI_GAME){
            platformStr = PlatformValue.HUAWEI;//华为快游戏
        }else if(os === cc.sys.OS_WINDOWS){
            platformStr = "windows";
        }

        return platformStr;
    }
}
