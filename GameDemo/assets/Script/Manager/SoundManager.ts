/*
 * @Description: 音频管理器
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-03 21:02:11
 * @LastEditors: CoderXZ
 * @LastEditTime: 2020-05-28 22:42:51
 */
import EventManager from "./EventManager";
import ResManager from "./ResManager";

export interface EventObject{
    eventId:number,
    eventName:string,
    handler:Function,
    once:boolean,
    target:Object,
};

export default class SoundManager{

    private static m_instance:SoundManager = null;

    public static getInstance():SoundManager{
        if(this.m_instance === null){
            this.m_instance = new SoundManager();
        }

        return this.m_instance;
    }

    private constructor(){
        cc.log('SoundManager init()');
        EventManager.getInstance().on("stopMusic",()=>{
            cc.log("on stopMusic");
        });
    }

    playEffect(url:string,isLoop:boolean = false){
        let effectUrl = 'sound/effect/' + url;
        cc.loader.loadRes(effectUrl, cc.AudioClip, function (err, clip) {
            var audioID = cc.audioEngine.playEffect(clip, isLoop);
        });

        // ResManager.getInstance().loadRes(effectUrl, cc.AudioClip, function (err, clip) {
        //     var audioID = cc.audioEngine.playEffect(clip, isLoop);
        // });
    }

    playMusic(url:string,isLoop:boolean = true){
        let musicUrl = 'sound/music/' + url;
        cc.loader.loadRes(musicUrl, cc.AudioClip, function (err, clip) {
            var audioID = cc.audioEngine.playMusic(clip, isLoop);
        });

        // ResManager.getInstance().loadRes(musicUrl, cc.AudioClip, function (err, clip) {
        //     var audioID = cc.audioEngine.playMusic(clip, isLoop);
        // });
    }

    stopMusic(){
        cc.audioEngine.stopMusic();
    }

}
