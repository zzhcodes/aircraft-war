/*
 * @Description: 
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-13 23:09:37
 * @LastEditors: 周曾辉
 * @LastEditTime: 2020-04-13 23:49:02
 */

import Global from "../Global";
import HttpRequest from "./HttpRequest";


const {ccclass, property} = cc._decorator;

@ccclass
export default class HttpManager {

    private static m_instance:HttpManager = null;
    public static getInstance():HttpManager{
        if(!this.m_instance){
            this.m_instance = new HttpManager();
        }

        return this.m_instance;
    }

    private constructor(){

    }

    public sendRequest(url, callfunc, params, ex_data){
        let requets = {url:url, callfunc : callfunc, params : params, ex_data : ex_data}
        let req = new HttpRequest(requets, this);
        req.send();
    }

    public sendUrl(data:any, packet:any, callfunc:Function, ex_data:any){
        if(!ex_data){
            ex_data = {};
        }
        Global.onSendUrl(data, ex_data);
        data.param = encodeURI(JSON.stringify(packet).toString())
        let parseStr1 =","
        let parseStr2 = ":"
        data.param = data.param.toString().replace(/,/g, '%' + parseStr1.charCodeAt(0).toString(16).toLocaleUpperCase())
        data.param = data.param.toString().replace(/:/g, '%' + parseStr2.charCodeAt(0).toString(16).toLocaleUpperCase())

        //url是服务器地址，这里分测试服和正式服
        let url = Global.getHttpUrlByChannelName();
        this.sendRequest(url, callfunc, data, ex_data)
    }
}
