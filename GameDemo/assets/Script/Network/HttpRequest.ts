/*
 * @Description: 
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-13 23:14:46
 * @LastEditors: 周曾辉
 * @LastEditTime: 2020-04-13 23:55:29
 */

import GlobalFunction from "../Utils/GlobalFunction";


const {ccclass, property} = cc._decorator;

@ccclass
export default class HttpRequest {

    private m_url:string = "";

    private m_request:any = null;

    private m_agent:any = null;

    private m_params:any = null;

    constructor(request, agent){
        var self = this;
        self.m_request = request;
        self.m_agent = agent;
    
        let url = request.url;
        self.m_params = request.params;
    
        if (!self.m_request.ex_data.post) {
            let first = true;
            for (var i in self.m_params) {
                if (first) {
                    first = false;
                    url = url + "?"
                } else {
                    url = url + "&"
                }
                url = url + i + "=" + self.m_params[i];
            }
        }
    
        cc.log("发送地址: ", url)
        this.m_url = url;
        return self;
    }

    
    public send(){
        var self = this;
        var xhr = cc.loader.getXMLHttpRequest();
        xhr.onreadystatechange = function() {
            clearTimeout(timer);
            cc.log("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa 111", xhr.readyState);
            cc.log("xhr.status ------  ", xhr.status);
    
            if (xhr.readyState === 4 && (xhr.status >= 200 && xhr.status < 300)) {
                var respText = xhr.responseText;
                var ret = null;
                try {
                    let jsonStr = GlobalFunction.xorfunc(xhr.responseText);
                    cc.log("Request.prototype.send  111", JSON.stringify(jsonStr))
                    ret = JSON.parse(jsonStr);
                    self.onfinished(self.m_request, ret)
                } catch (e) {
                    ret = {
                        ret: -10001,
                        errmsg: e
                    };
                    cc.log("Request.prototype.send ", JSON.stringify(ret), "self.m_url = ", self.m_url)
                }
            } else if (xhr.readyState === 4) {
                // if (xhr.hasRetried) {
                //     return;
                // }
                setTimeout(function() {
                    retryFunc();
                }, 5000);
            } else {}
        };
    
        if (self.m_request.ex_data.post) {
            xhr.open("POST", self.m_url, true);
        } else {
            xhr.open("GET", self.m_url, true);
        }
    
        if (cc.sys.isNative) {
            // xhr.setRequestHeader("Accept-Encoding", "gzip,deflate", "text/html;charset=UTF-8");
            xhr.setRequestHeader("Accept-Encoding", "gzip,deflate");
        }
    
        var retryFunc = function() {
            self.send();
        };
    
        var timer = setTimeout(function() {
            // xhr.hasRetried = true;
            xhr.abort();
            retryFunc();
        }, 5000);
    
        xhr.timeout = 5000;
    
        try {
            xhr.send();
        } catch (e) {
            retryFunc();
        }
    
        //showLoading();
    }
    
    public onfinished(request, response) {
        console.log("http response --- " + JSON.stringify(response));
        if (response) {
            if (response.ret == 505) {
                //skey错误, 不用回调了;
                //掉线

                // showTip("您长期未操作，点击确定重新登陆。", reLink);
    
                // function reLink() {
                //     cc.audioEngine.stopAll();
                //     cc.game.restart();
                // }
    
                return;
            }
        }
    
        if (request.catch) {
            var info = ""
            if (response) {
                if (response.ret != 0) {
                    info = response.desc
                    response = null;
                }
            } else {
                info = "请求失败"
            }
    
            if (info != "") {
                // 提示消息;
                //TODO
                cc.log("提示消息: ", info)
            }
    
            if (!response) {
                // 提示消息，网络异常，请检查网络是否可用;
                //TODO
            }
        }
    
        if (request.callfunc) {
            if (request.single) {
    
            } else {
                request.callfunc(response)
                    //hideLoading();
            }
        }
    }
}
