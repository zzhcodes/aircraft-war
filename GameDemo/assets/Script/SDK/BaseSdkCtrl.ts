/*
 * @Description: 
 * @version: 
 * @Author: CoderXZ
 * @Date: 2020-05-28 22:43:21
 * @LastEditors: CoderXZ
 * @LastEditTime: 2020-05-28 22:50:23
 */ 
import BaseSdkInterface from "./BaseSdkInterface";

export default class BaseSdkCtrl implements BaseSdkInterface {
    m_bannerAd: any = null;
    m_videoAd: any = null;
    m_insertScreenAd: any = null;
    m_platformStr: string = "";
    authorize(): void {
        throw new Error("Method not implemented.");
    }
    init(): void {
        throw new Error("Method not implemented.");
    }
    login(): void {
        throw new Error("Method not implemented.");
    }
    share(title: string, imgUrl: string, queryStr: string = "", imgUrlId: string = ""): void {
        throw new Error("Method not implemented.");
    }
}
