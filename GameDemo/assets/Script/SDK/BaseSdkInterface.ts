/*
 * @Description: 
 * @version: 
 * @Author: CoderXZ
 * @Date: 2020-04-08 08:22:05
 * @LastEditors: CoderXZ
 * @LastEditTime: 2020-05-28 22:15:26
 */



export default interface BaseSdkInterface {

    m_platformStr: string;

    m_bannerAd:any;

    m_videoAd:any;

    m_insertScreenAd:any;

    init(): void;

    authorize(): void;

    login(): void;

    share(title: string, imgUrl: string, queryStr: string, imgUrlId: string): void;



}
