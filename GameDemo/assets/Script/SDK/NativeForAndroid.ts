/*
 * @Description: 
 * @version: 
 * @Author: CoderXZ
 * @Date: 2020-09-07 21:16:28
 * @LastEditors: CoderXZ
 * @LastEditTime: 2020-09-07 22:48:53
 */


export default class NativeForAndroid {


    public static showToast(str:string){
        jsb.reflection.callStaticMethod("org/cocos2dx/javascript/Utils", "showToast", "(Ljava/lang/String;)V", str);
    }

    public static showLog(str:string){
        jsb.reflection.callStaticMethod("org/cocos2dx/javascript/Utils", "showLog", "(Ljava/lang/String;)V", str);
    }
}
