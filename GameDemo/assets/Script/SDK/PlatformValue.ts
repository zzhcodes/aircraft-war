/*
 * @Description: 平台常量字符串值
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-03 21:02:11
 * @LastEditors: CoderXZ
 * @LastEditTime: 2020-09-07 21:19:32
 */

export const PlatformValue:any = {
    WECHAT:"wechat_smallgame",//微信小游戏
    QQ:"qq_smallgame",//QQ小游戏
    BAIDU:"baidu_smallgame",//百度小游戏
    XIAOMI:"xiaomi_smallgame",//小米小游戏
    HUAWEI:"huawei_quickgame",//华为快游戏
    IOS:"ios",//ios
    WINDOWS:"windows",
}
