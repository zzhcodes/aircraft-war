/*
 * @Description: 
 * @version: 
 * @Author: CoderXZ
 * @Date: 2020-04-08 08:22:05
 * @LastEditors: CoderXZ
 * @LastEditTime: 2020-05-28 22:49:17
 */
import BaseSdkCtrl from "./BaseSdkCtrl";



export default class WechatApi extends BaseSdkCtrl {

    m_platformStr = "wechat";

    init() {
        if (wx) {

            //显示右上角的分享按钮
            wx.showShareMenu({
                withShareTicket: true
            })


            //监听右上角转发  回调
            wx.onShareAppMessage(function () {
                // 用户点击了“转发”按钮
                return {
                    title: '转发标题'
                }
            })

            //回到前台
            wx.onShow(res){
                if (res) {
                    console.log(res);
                }
            }

            //回到后台
            wx.onHide(res){
                if (res) {
                    console.log(res);
                }
            }

        }
    }

    authorize() {

    }

    //登录认证
    login() {
        if (wx) {
            wx.login({
                success(res) {
                    if (res.code) {
                        //发起网络请求
                        wx.request({
                            url: 'https://test.com/onLogin',
                            data: {
                                code: res.code
                            }
                        })
                    } else {
                        console.log('登录失败！' + res.errMsg)
                    }
                }
            })
        }
    }

    //分享
    share(title: string, imgUrl: string, queryStr: string = "", imgUrlId: string = "") {
        if (wx) {
            wx.shareAppMessage({
                imageUrlId: title,
                imageUrl: imgUrl,
                query: queryStr,
                imageUrlId: imgUrlId
            });
        }
    }

    //激励视频广告 只能用户关闭广告
    showVideoAd(adUnitId: string) {
        if (wx) {
            this.m_videoAd = wx.createRewardedVideoAd({ adUnitId: adUnitId });

            this.m_videoAd.onLoad(() => {
                console.log('激励视频 广告加载成功');
                this.m_videoAd.show().then(() => {
                    console.log('激励视频 广告显示');
                })
            })


            this.m_videoAd.onError(err => {
                console.log(err)
            })

            this.m_videoAd.onClose(res => {
                // 用户点击了【关闭广告】按钮
                // 小于 2.1.0 的基础库版本，res 是一个 undefined
                if (res && res.isEnded || res === undefined) {
                    // 正常播放结束，可以下发游戏奖励
                }
                else {
                    // 播放中途退出，不下发游戏奖励
                }
            })
        }
    }

    createBannerAd(adUnitId: string, adInterval: number = 30) {
        if (wx) {
            this.m_bannerAd = wx.createBannerAd({
                adUnitId: adUnitId,
                adIntervals: adInterval, // 自动刷新频率不能小于30秒s
                style: {
                    left: 10,
                    top: 76,
                    width: 320
                }
            })

            this.m_bannerAd.onError(err => {
                console.log(err)
            })

            this.m_bannerAd.onLoad(() => {
                console.log('banner 广告加载成功')
                // this.m_bannerAd.show().then(() => {
                //     console.log('banner 广告显示')
                // })

                this.m_bannerAd.show()
                    .catch(err => {
                        this.m_bannerAd.load()
                            .then(() => this.m_bannerAd.show())
                    })
            }
        }
    }

    //销毁banner广告
    destroyBannerAd(){
        if(this.m_bannerAd){
            this.m_bannerAd.destroy();
            this.m_bannerAd = null;
        }
    }

    // 显示banner广告
    public showBannerAd() {
        if (this.m_bannerAd) {
            this.m_bannerAd.show();
        }
    }

    // 隐藏banner广告
    public hideBannerAd() {
        if (this.m_bannerAd) {
            this.m_bannerAd.hide();
        }
    }

    //插屏广告  待加入


}
