/*
 * @Description: 工具类
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-03 22:46:06
 * @LastEditors: CoderXZ
 * @LastEditTime: 2020-05-24 23:48:17
 */

import ResManager from "../Manager/ResManager";
import BasePanel from "../Component/BasePanel";
import EventManager from "../Manager/EventManager";
import { EventName } from "../Const/EventName";


const {ccclass, property} = cc._decorator;

@ccclass
export default class Utils{
    
    public static loadSpriteFrame(node:cc.Node,spriteUrl:string):void{
        ResManager.getInstance().loadRes(spriteUrl,cc.SpriteFrame,(err,spriteFrame)=>{
            if(!err){
                let spriteCom = node.getComponent(cc.Sprite);
                if(!spriteCom){
                    spriteCom = node.addComponent(cc.Sprite);
                }
                node.getComponent(cc.Sprite).spriteFrame = spriteFrame;
            }else{
                cc.log("loadSpriteFrame error,spriteFrame url:",spriteUrl);
            }
            
        })
    }

    public static loadPrefab(url:string,callback:Function = null,data:any = null,parent:cc.Node = null,zIndex:number = 9999):void{
        let prefabUrl = 'prefabs/' + url;
        cc.log("loadPrefab prefab url:" + prefabUrl);
    
        ResManager.getInstance().loadRes(prefabUrl,cc.Prefab,(err,prefab)=>{
            if(!err){
                let prefabCom = cc.instantiate(prefab);

                let prefabName = prefabCom.name;
                
                let viewNode = prefabCom.getComponent(BasePanel);
                
                if(viewNode === null){
                    let scriptClass = cc.js.getClassByName(prefabName);
                    if(scriptClass){
                        viewNode = prefabCom.addComponent(scriptClass);
                    }
                }
    
                if(viewNode){
                    viewNode.setData && viewNode.setData(data);
                }
                
    
                let canvas = cc.find("Canvas")
                if(!parent){
                    if(canvas){
                        canvas.addChild(prefabCom,zIndex);
                    }
                }else{
                    parent.addChild(prefabCom,zIndex);
                }
    
                if(callback){
                    callback();
                }
    
            }else{
                cc.log("loadPrefab error!name:",prefabUrl);
            }
            
            

        })
    }

    public static loadAnimationPrefab(url:string,parent:cc.Node,zIndex:number = 9999):void{
        let prefabUrl = 'animation/' + url;
        cc.log("loadAnimationPrefab prefab url:" + prefabUrl);
        ResManager.getInstance().loadRes(prefabUrl,cc.Prefab,(err,prefab)=>{
            if(!err){
                let prefabCom = cc.instantiate(prefab);
                parent.addChild(prefabCom,zIndex);
            }else{
                cc.log("loadAnimationPrefab error,name:",prefabUrl);
            }
            
        })
    }

    public static loadSpriteAltasFrame(node:cc.Node,altasUrl:string,spriteUrl:string):void{
        ResManager.getInstance().loadRes(spriteUrl,cc.SpriteAtlas,(err,spriteAltas)=>{

            if(!err){
                let spriteFrame = spriteAltas.getFrame(spriteUrl);
                if(!spriteFrame){
                    cc.log("loadSpriteAltasFrame failed! altas:" + altasUrl + ",sprite:" + spriteUrl);
                    return;
                }
                let spriteCom = node.getComponent(cc.Sprite);
                if(!spriteCom){
                    spriteCom = node.addComponent(cc.Sprite);
                }
                node.getComponent(cc.Sprite).spriteFrame = spriteFrame;
            }else{
                cc.log("loadSpriteAltasFrame error,altas url:%s,spriteUrl:%s",altasUrl,spriteUrl);
            }
        })
    }
}
