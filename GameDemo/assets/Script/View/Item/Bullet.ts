/*
 * @Description: 炸弹类
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-06 18:28:22
 * @LastEditors: 周曾辉
 * @LastEditTime: 2020-04-25 17:28:36
 */

import EventManager from "../../Manager/EventManager";
import { EventName } from "../../Const/EventName";
import SoundManager from "../../Manager/SoundManager";
import PlayView from "../Layer/PlayView";
import CheckPointMgr from "../../Data/DataMgr/CheckPointMgr";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Bullet extends cc.Component {

    m_updateMove:boolean = false;
    m_Speed:number = 0;

    onLoad () {
        this.initData();
    }

    start () {

    }

    initData(){
        this.m_updateMove = false;
        this.m_Speed = CheckPointMgr.getInstance().getBulletSpeed();
    }

    init(){
        this.m_updateMove = false;
    }

    onCollisionEnter(other,self) {
        cc.log("Bullet 发生碰撞啦！");
        gGameCtl.onBulletKilled(this.node);
    }

    setSecondPos(pos){
        var seq = cc.sequence(
            cc.moveTo(0.1,pos),
            cc.callFunc(function(){
                this.m_updateMove = true;
                SoundManager.getInstance().playEffect('bullet');
            }.bind(this))
        );
        this.node.runAction(seq);
    }
    
    update (dt) {
        if(this.m_updateMove){
            var y = this.node.y;
            y += this.m_Speed*dt;
            this.node.y = y;
            if( this.m_Speed == 0 ){

                cc.log('速度:')
            } 
            if( y > 920 ){
                this.m_updateMove = false;
                gGameCtl.onBulletKilled(this.node);
            }
        }
    }
}
