/*
 * @Description: 
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-19 23:09:41
 * @LastEditors: 周曾辉
 * @LastEditTime: 2020-04-19 23:11:50
 */


const {ccclass, property} = cc._decorator;

@ccclass
export default class CheckPointItem extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {

    }

    setData(data:any){
        this.label.string = "第"  + data + "关";
    }

    // update (dt) {}
}
