/*
 * @Description: 
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-06 18:18:06
 * @LastEditors: 周曾辉
 * @LastEditTime: 2020-04-22 23:25:00
 */

import GlobalFunction from "../../Utils/GlobalFunction";
import EventManager from "../../Manager/EventManager";
import { EventName } from "../../Const/EventName";
import Utils from "../../Utils/Utils";
import SoundManager from "../../Manager/SoundManager";
import UserData from "../../UserData/UserData";
import CheckPointMgr from "../../Data/DataMgr/CheckPointMgr";


const { ccclass, property } = cc._decorator;

@ccclass
export default class Enemy extends cc.Component {

    m_isDead:boolean = false;
    // LIFE-CYCLE CALLBACKS:


    m_updateMove: boolean = false;
    m_Speed: number = 0;

    onLoad() {
        this.initData();
    }

    start() {

    }

    initData() {
        this.m_updateMove = false;
        this.m_Speed = CheckPointMgr.getInstance().getEnemySpeed();
    }

    init() {
        this.m_isDead = false;
        this.m_updateMove = false;

        let parent = this.node.parent;
        this.node.y = parent.y + cc.winSize.height / 2;

        let xMin = parent.x - cc.winSize.width / 2 + 30;//不能太靠边
        let xMax = parent.x + cc.winSize.width / 2 - 30;//不能太靠边

        this.node.x = GlobalFunction.getRandomNum(xMin, xMax);

        this.m_updateMove = true;
    }

    onCollisionEnter(other, self) {
        if (!this.m_isDead) {
            this.m_isDead = true;
            this.node.stopAllActions();
            Utils.loadAnimationPrefab('explostionAni', this.node);
            SoundManager.getInstance().playEffect('boom');

            

            this.scheduleOnce(() => {
                this.node.removeAllChildren();
                gGameCtl.onEnemyKilled(this.node);
            }, 0.5);
        }
    }

    update(dt) {
        if (this.m_updateMove) {
            var y = this.node.y;
            y -= this.m_Speed * dt;
            this.node.y = y;
           
            let yMin = this.node.parent.y - cc.winSize.height / 2 - 30;
            if (y <= yMin) {
                this.m_updateMove = false;
                gGameCtl.onEnemyKilled(this.node);
            }
        }
    }

}
