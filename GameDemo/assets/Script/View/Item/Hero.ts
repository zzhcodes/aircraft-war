/*
 * @Description: 
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-06 17:23:44
 * @LastEditors: 周曾辉
 * @LastEditTime: 2020-04-12 00:18:19
 */
import EventManager from "../../Manager/EventManager";
import { EventName } from "../../Const/EventName";
import Utils from "../../Utils/Utils";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Hero extends cc.Component {

    
    _canMove:boolean = false;
    _startGame:boolean = false;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.initEvent();
        this.initData();
    }

    start () {

    }

    initData(){
        this._canMove = false;
        this._startGame = false;
    }

    initEvent(){
        this.node.on("touchstart",(evtData:cc.Event.EventTouch)=>{

            if(!this._startGame){
                return;
            }

            EventManager.getInstance().emit(EventName.CONTINUE_GAME);
            this._canMove = true;
        });

        this.node.on("touchmove",(evtData:cc.Event.EventTouch)=>{

            if(!this._startGame){
                return;
            }

            if(this._canMove){
                let curPos:cc.Vec2 = evtData.getLocation();

                let parent = this.node.parent;
                let pos = parent.convertToNodeSpaceAR(curPos);
                this.node.setPosition(pos);
            
            }
        });

        this.node.on("touchend",(evtData:cc.Event.EventTouch)=>{
            if(!this._startGame){
                return;
            }

            this._canMove = false;
            EventManager.getInstance().emit(EventName.PAUSE_GAME);
        });

        this.node.on("touchcancel",(evtData:cc.Event.EventTouch)=>{
            if(!this._startGame){
                return;
            }
            this._canMove = false;
            EventManager.getInstance().emit(EventName.PAUSE_GAME);
            // this._startGame = false;
            // cc.director.pause;
            
        });

        EventManager.getInstance().on(EventName.START_GAME,()=>{
            this._startGame = true;
        })

        EventManager.getInstance().on(EventName.GAME_OVER,()=>{
            this._startGame = false;
        })

        EventManager.getInstance().on(EventName.CONTINUE_GAME,()=>{
            this._startGame = true;
        })
    }

    onCollisionEnter(other, self){
        EventManager.getInstance().emit(EventName.GAME_OVER);
    }

    onCollisionStay(other, self){
 
    }

    onCollisionExit(other, self){

    }
    
    // update (dt) {}
}
