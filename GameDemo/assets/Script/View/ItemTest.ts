

const {ccclass, property} = cc._decorator;

@ccclass
export default class ItemTest extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;


    onLoad () {

    }

    start () {

    }

    setData(param:any){
        this.label.string = "第" + param + "个item";
    }

    // update (dt) {}
}
