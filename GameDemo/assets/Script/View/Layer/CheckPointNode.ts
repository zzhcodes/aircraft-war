/*
 * @Description: 
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-19 23:02:45
 * @LastEditors: 周曾辉
 * @LastEditTime: 2020-04-22 22:57:45
 */

import CfgTableMgr from "../../Data/CfgTableMgr";
import CheckPointItem from "../Item/CheckPointItem";


const {ccclass, property} = cc._decorator;

@ccclass
export default class CheckPointNode extends cc.Component {

    @property(cc.Node)
    layout: cc.Node = null;


    @property({
        type:cc.Prefab,
        tooltip:"选关的prefab",
    })
    checkPointItem: cc.Prefab = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.initView();
    }

    start () {

    }

    initView(){
        let checkPointData = CfgTableMgr.getInstance().getTableData("CheckPoint");
        cc.log("checkPointData:" + checkPointData);
        
        if(!checkPointData){
            return;
        }
        
        for(let key in checkPointData){
            let checkItemPrefab = cc.instantiate(this.checkPointItem);
            checkItemPrefab.getComponent(CheckPointItem).setData(key);
            this.layout.addChild(checkItemPrefab);
        }
    }

    // update (dt) {}
}
