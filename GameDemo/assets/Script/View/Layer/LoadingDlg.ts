/*
 * @Description: loading 界面
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-15 22:57:17
 * @LastEditors: 周曾辉
 * @LastEditTime: 2020-04-16 23:34:27
 */

import BasePanel from "../../Component/BasePanel";
import { PanelType, PanelId } from "../../Const/PanelValue";
import EventManager from "../../Manager/EventManager";
import { EventName } from "../../Const/EventName";


const {ccclass, property} = cc._decorator;

@ccclass
export default class LoadingDlg extends BasePanel {

    @property(cc.Node)
    loadSprite:cc.Node = null;

    onLoad () {
        super.onLoad();
        this.loadSprite.stopAllActions();
        let ation = cc.repeatForever(cc.rotateBy(1,360));
        this.loadSprite.runAction(ation);
    }

    constructor(){
        super();
        this._panelId = PanelId.LoadingDlg;
        this._panelType = PanelType.None;
        this.initEvent();
    }

    initEvent(){
        this.bindEvent(EventName.HIDE_LOADING,this.onHideLoading.bind(this));
    }

    onHideLoading(){
        this.close();
    }

    start () {
        super.start();
    }

    onDestroy(){
        super.onDestroy();
        this.loadSprite.stopAllActions();
    }

    // update (dt) {}
}
