/*
 * @Description: 
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-06 16:29:24
 * @LastEditors: CoderXZ
 * @LastEditTime: 2020-06-07 20:53:32
 */

import Utils from "../../Utils/Utils";
import EventManager from "../../Manager/EventManager";
import { EventName } from "../../Const/EventName";
import { PanelId, PanelType } from "../../Const/PanelValue";
import BasePanel from "../../Component/BasePanel";
import UserData from "../../UserData/UserData";
import CheckPointMgr from "../../Data/DataMgr/CheckPointMgr";
import GlobalFunction from "../../Utils/GlobalFunction";

enum GameState{
    AT_HOME = 1,//在大厅
    PLAYING,//在打飞机中
    BUILDING,//在基地 升级
}

const { ccclass, property } = cc._decorator;

@ccclass
export default class MainPanel extends BasePanel {
    // LIFE-CYCLE CALLBACKS:

    @property(cc.Node)
    startGameBtn: cc.Node = null;

    @property(cc.Label)
    numLabel: cc.Label = null;

    @property(cc.Label)
    checkPointNumLabel: cc.Label = null;

    @property(cc.Node)
    scoreNode: cc.Node = null;

    @property(cc.Node)
    checkPointNumNode: cc.Node = null;

    //基地按钮
    @property(cc.Node)
    buildUpBtn: cc.Node = null;

    @property(cc.Node)
    buildUpNode: cc.Node = null;

    m_curState:GameState = GameState.AT_HOME;

    constructor() {
        super();
        this._panelId = PanelId.MainPanel;
        this._panelType = PanelType.Panel;
    }

    onLoad() {
        cc.log("MainPanel loaded");

        this.initEvent();

        this.initData();
        this.initView();
    }

    start() {
        GlobalFunction.showTip("终于进来了！");
    }

    initData(){
        this.m_curState = GameState.AT_HOME;
    }

    initView() {
        this.showKillEnemyNum();
        this.showCurCheckPointNum();

        this.buildUpNode.active = false;
        this.startGameBtn.active = this.m_curState === GameState.AT_HOME;
        this.buildUpBtn.active = this.m_curState === GameState.AT_HOME;
    }

    initEvent() {
        this.bindEvent(EventName.GAME_OVER, this.onGameOver.bind(this));
        this.bindEvent(EventName.CONTINUE_GAME, this.onContinueGame.bind(this));
        this.bindEvent(EventName.UPDATE_KILLNUM, this.onUpdateKillEnemyNum.bind(this));
        this.bindEvent(EventName.PASS_CHECKPOINT, this.onPassCheckPoint.bind(this));
    }

    onGameOver() {

        this.m_curState = GameState.AT_HOME;

        let data: any = {};
        data['desc'] = "游戏结束，是否续命继续游戏？";
        data['okCallFunc'] = () => {
            EventManager.getInstance().emit(EventName.CONTINUE_GAME);
        }

        EventManager.getInstance().emit(EventName.SHOW_PANEL, { panelId: PanelId.PopWindow, data: data });

        this.scheduleOnce(() => {
            this.startGameBtn.active = true;
            this.buildUpBtn.active = true;
        }, 1);

        // cc.director.pause();
    }

    onStartGame(evt: Event, customData: any) {
        cc.log("startGame Btn clicked");
        EventManager.getInstance().emit(EventName.START_GAME);
        this.m_curState = GameState.PLAYING;
        
        this.startGameBtn.active = false;
        this.buildUpBtn.active = false;
        this.scoreNode.active = !(this.startGameBtn.active);
        this.checkPointNumNode.active = !(this.startGameBtn.active);
        
    }

    onContinueGame() {
        this.m_curState = GameState.PLAYING;
        this.startGameBtn.active = false;
        this.buildUpBtn.active = false;
    }

    onUpdateKillEnemyNum() {
        this.showKillEnemyNum();
    }

    showKillEnemyNum() {
        this.scoreNode.active = this.m_curState === GameState.PLAYING;
        let num = UserData.getInstance().getKillEnemyNum();
        this.numLabel.string = num + "个";
    }

    showCurCheckPointNum() {
        
        this.checkPointNumNode.active = this.m_curState === GameState.PLAYING;
        let num = UserData.getInstance().getCheckPointNum();
        this.checkPointNumLabel.string = num + "";
    }

    onPassCheckPoint(eventData) {
        // let curCheckNum = Number(eventData);
        // let maxNum = CheckPointMgr.getInstance().getMaxCheckPointNum();
        // if (curCheckNum <= maxNum) {

        // } else {
        //     cc.log("恭喜你，已经通关！");
        // }
    }

    onClickJiDi(){
        cc.log("进入基地");
        this.m_curState = GameState.BUILDING;
        this.buildUpNode.active = this.m_curState === GameState.BUILDING;
    }

    onClickBackHome(){
        cc.log("回到主界面");
        this.m_curState = GameState.AT_HOME;
        this.buildUpNode.active = this.m_curState === GameState.BUILDING;
    }

    // update (dt) {}
}
