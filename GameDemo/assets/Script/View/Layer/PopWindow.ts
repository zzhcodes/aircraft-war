/*
 * @Description: 
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-07 23:05:25
 * @LastEditors: 周曾辉
 * @LastEditTime: 2020-04-17 00:14:17
 */

import BasePanel from "../../Component/BasePanel";
import { PanelType, PanelId } from "../../Const/PanelValue";


const {ccclass, property} = cc._decorator;

@ccclass
export default class PopWindow extends BasePanel {

    @property(cc.Label)
    label: cc.Label = null;

    okCallFunc:Function = null;
    constructor(){
        super();
        this._panelId = PanelId.PopWindow;
        this._panelType = PanelType.Window;
    }

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        super.onLoad();
    }

    setData(data:any){
        super.setData(data);
        this.label.string = this._data['desc'];
        this.okCallFunc = this._data['okCallFunc'];
    }

    onOk(){
        cc.log("你点击了确定");
        this.okCallFunc();
        this.hide();
    }

    onCancel(){
        cc.log("你点击了取消");
        this.hide();
    }

    // update (dt) {}
}
