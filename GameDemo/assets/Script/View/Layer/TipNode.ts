/*
 * @Description: 
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-25 22:01:34
 * @LastEditors: 周曾辉
 * @LastEditTime: 2020-04-26 00:06:37
 */


const {ccclass, property} = cc._decorator;

@ccclass
export default class TipNode extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;


    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.node.active = true;
    }


    setData(content:string){
        this.label.string = content;

        let callFunc = cc.callFunc(()=>{
            this.node.destroy();
        })
        let action = cc.sequence(cc.delayTime(0.2),cc.moveBy(0.5,0,250),cc.fadeOut(0.2),callFunc);
        this.node.runAction(action);
    }

    // update (dt) {}
}
