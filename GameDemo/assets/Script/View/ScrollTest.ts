/*
 * @Description: 
 * @version: 
 * @Author: 周曾辉
 * @Date: 2020-04-06 14:59:38
 * @LastEditors: 周曾辉
 * @LastEditTime: 2020-04-16 23:36:32
 */

import Global from "../Global";


const {ccclass, property} = cc._decorator;

@ccclass
export default class ScrollTest extends cc.Component {

    @property(cc.Prefab)
    itemPrefab: cc.Prefab = null;

    @property(cc.ScrollView)
    scrollView: cc.ScrollView = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.log("ScrollTest loaded");
        
        this.initView();
    }

    start () {

    }

    initView(){
        
        for(let i = 0; i < 60; i++){
            var itemCopy = cc.instantiate(this.itemPrefab);
            itemCopy.parent = this.scrollView.content;
            itemCopy.active = true;
            itemCopy.getComponent("ItemTest").setData(i + 1);
        }

        this.scheduleOnce(()=>{
            this.scrollView.getComponent('ScrollViewPro').optDC();
        },1);
        

        let children = this.scrollView.content.children;
        cc.log("length of scrollView's children:", children.length);
    }s

    // update (dt) {}
}
